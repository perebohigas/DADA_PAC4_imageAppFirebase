package edu.uoc.android.imageapp.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {

    private static final String TAG = FileUtils.class.getSimpleName();

    // File information
    public static final String FOLDER_IMAGE_PATH_NAME = "UOCImageApp";
    public static final String IMAGE_FILE_NAME = "imageapp.jpg";

    /**
     * Checks if external storage is available for read and write
     */
    public static boolean isExternalStorageAvailable() {
        return !isExternalStorageReadable() || !isExternalStorageWritable();
    }

    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Method to create an image file.
     *
     * @return file
     */
    public static File createImageFile(String storageDir, String fileName, Bitmap bitmap) {
        try {
            // create a File object for the parent directory
            File storageDirFile = new File(storageDir);
            // have the object build the directory structure, if needed.
            storageDirFile.mkdirs();
            // create a File object for the output file
            File imageFile = new File(storageDirFile, fileName);
            // set bitmap
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            // write data in file
            FileOutputStream fos = new FileOutputStream(imageFile);
            fos.write(stream.toByteArray());
            fos.close();
            return imageFile;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    /**
     * Method to remove an image file.
     *
     * @return true if file has been delete, false if not
     */
    public static boolean deleteFile(String storageDir, String fileName) {
        // create a File object for the output file
        File imageFile = new File(storageDir, fileName);
        // delete file if it exists
        if (imageFile.exists()) {
            return imageFile.delete();
        }
        return false;
    }

    /**
     * Method to check if the image file exists
     *
     * @return true if file exists, false if not
     */
    public static boolean existFile(String storageDir, String fileName) {
        // create a File object for the output file
        File imageFile = new File(storageDir, fileName);
        // check if this file exists
        return imageFile.exists();
    }

    /**
     * Method to get the image file
     *
     * @return file
     */
    public static File getFile(String storageDir, String fileName) {
        // check if this file exists
        if (existFile(storageDir, fileName)) {
            // create a File object for the output file
            File imageFile = new File(storageDir, fileName);
            return imageFile;
        } else {
            return null;
        }
    }
}
